<?php

/**
 * @file
 * Integrates payonline redirected payment service.
 */


/**
 * Implements hook_menu().
 */
function uc_payonline_menu() {
  $items = array();

  $items['cart/payonline/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_payonline_complete',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uc_payonline.pages.inc',
  );
  
  $items['cart/payonline/fail'] = array(
    'title' => 'Order failed',
    'page callback' => 'uc_payonline_fail',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'uc_payonline.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function uc_payonline_init() {
  global $conf;
  $conf['i18n_variables'][] = 'uc_payonline_method_title';
  $conf['i18n_variables'][] = 'uc_payonline_checkout_button';
}

/**
 * Implements hook_ucga_display().
 */
function uc_payonline_ucga_display() {
  // Tell UC Google Analytics to display the e-commerce JS on the custom
  // order completion page for this module.
  if (arg(0) == 'cart' && arg(1) == 'payonline' && arg(2) == 'complete') {
    return TRUE;
  }
}

/**
 * Implements hook_uc_payment_method().
 *
 * @see uc_payment_method_payonline()
 */
function uc_payonline_uc_payment_method() {
  $path = base_path() . drupal_get_path('module', 'uc_payonline');
  $title = variable_get('uc_payonline_method_title', t('Credit card on a secure server:'));
  $title .= '<br />' . theme('image', array(
    'path' => drupal_get_path('module', 'uc_payonline') . '/payonline.png',
    'attributes' => array('class' => array('uc-payonline-logo')),
  ));

  $methods['payonline'] = array(
    'name' => t('payonline'),
    'title' => $title,
    'review' => variable_get('uc_payonline_check', FALSE) ? t('Credit card/eCheck') : t('Credit card'),
    'desc' => t('Redirect to payonline to pay by credit card or eCheck.'),
    'callback' => 'uc_payment_method_payonline',
    'redirect' => 'uc_payonline_form',
    'weight' => 3,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Adds payonline settings to the payment method settings form.
 *
 * @see uc_payonline_uc_payment_method()
 */
function uc_payment_method_payonline($op, &$order, $form = NULL, &$form_state = NULL) {
  switch ($op) {
    case 'cart-details':
      $build = array();

      if (variable_get('uc_payonline_check', FALSE)) {
        $build['pay_method'] = array(
          '#type' => 'select',
          '#title' => t('Select your payment type:'),
          '#default_value' => $_SESSION['pay_method'] == 'CK' ? 'CK' : 'CC',
          '#options' => array(
            'CC' => t('Credit card'),
            'CK' => t('Online check'),
          ),
        );
        unset($_SESSION['pay_method']);
      }

      return $build;

    case 'cart-process':
      if (isset($form_state['values']['panes']['payment']['details']['pay_method'])) {
        $_SESSION['pay_method'] = $form_state['values']['panes']['payment']['details']['pay_method'];
      }
      return;

    case 'settings':
      $form['uc_payonline_sid'] = array(
        '#type' => 'textfield',
        '#title' => t('Merchant number'),
        '#description' => t('Your payonline merchant account number.'),
        '#default_value' => variable_get('uc_payonline_sid', ''),
        '#size' => 16,
      );
      $form['uc_payonline_secret_word'] = array(
        '#type' => 'textfield',
        '#title' => t('Secret key for order verification'),
        '#description' => t('The secret key.'),
        '#default_value' => variable_get('uc_payonline_secret_word', ''),
        '#size' => 50,
      );
      $form['uc_payonline_language'] = array(
        '#type' => 'select',
        '#title' => t('Language preference'),
        '#description' => t('Adjust language on payonline pages.'),
        '#options' => array(
          'ru' => t('Russian'),
          'en' => t('English'),
        ),
        '#default_value' => variable_get('uc_payonline_language', 'ru'),
      );
      $form['uc_payonline_method_title'] = array(
        '#type' => 'textfield',
        '#title' => t('Payment method title'),
        '#default_value' => variable_get('uc_payonline_method_title', t('Credit card on a secure server:')),
      );
      $form['uc_payonline_method_title_icons'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show credit card icons beside the payment method title.'),
        '#default_value' => variable_get('uc_payonline_method_title_icons', TRUE),
      );
      $form['uc_payonline_checkout_button'] = array(
        '#type' => 'textfield',
        '#title' => t('Order review submit button text'),
        '#description' => t('Provide payonline specific text for the submit button on the order review page.'),
        '#default_value' => variable_get('uc_payonline_checkout_button', t('Submit Order')),
      );
      return $form;
  }
}

/**
 * Form to build the submission to payonline.com.
 */
function uc_payonline_form($form, &$form_state, $order) {
  $country = uc_get_country_data(array('country_id' => $order->billing_country));
  if ($country === FALSE) {
    $country = array(0 => array('country_iso_code_3' => 'USA'));
  }

  $page = variable_get('uc_cart_checkout_complete_page', '');

  $data = array(
    'MerchantId' => variable_get('uc_payonline_sid', ''),
    'Amount' => sprintf('%.2f', $order->order_total),
    'OrderId' => $order->order_id,
    'Currency' => 'RUB',
    //'demo' => variable_get('uc_payonline_demo', TRUE) ? 'Y' : 'N',
    //'ReturnUrl' => url($page, array('absolute' => TRUE)),
   // 'FailUrl' => url('cart/payonline/fail', array('absolute' => TRUE)),

  );
  
  if ($page) {
    $data['ReturnUrl'] = url($page, array('absolute' => TRUE));
  }
  
  $data['SecurityKey'] = uc_payonline_request_security_key($data);

  
  $form['#action'] = _uc_payonline_post_url();

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => variable_get('uc_payonline_checkout_button', t('Submit Order')),
  );

  return $form;
}

function uc_payonline_request_security_key($data) {
  $sec_key = variable_get('uc_payonline_secret_word', '');
  return md5("MerchantId=$data[MerchantId]&OrderId=$data[OrderId]&Amount=$data[Amount]&Currency=$data[Currency]&PrivateSecurityKey=$sec_key");

}

function uc_payonline_response_security_key($data) {
  $sec_key = variable_get('uc_payonline_secret_word', '');
  return md5("DateTime=$data[DateTime]&TransactionID=$data[TransactionID]&OrderId=$data[OrderId]&Amount=$data[Amount]&Currency=$data[Currency]&PrivateSecurityKey=$sec_key");

}

/**
 * Helper function to obtain payonline URL for a transaction.
 */
function _uc_payonline_post_url() {
  return 'https://secure.payonlinesystem.com/ru/payment/';
}
