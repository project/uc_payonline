<?php

/**
 * @file
 * payonline menu items.
 */
 
/**
 * Finalizes payonline transaction.
 */
function uc_payonline_fail() {
  watchdog('uc_payonline', "The payonline payment failed");
}


/**
 * Finalizes payonline transaction.
 */
function uc_payonline_complete() {
  
  watchdog('payonline', 'Receiving new order notification for order !order_id.', array('!order_id' => check_plain($_REQUEST['OrderId'])));

  $order = uc_order_load($_REQUEST['OrderId']);

  if ($order === FALSE || uc_order_status_data($order->order_status, 'state') != 'in_checkout') {
    watchdog('payonline', 'No such order: !order_id.', array('!order_id' => check_plain($_REQUEST['OrderId'])));
    return t('An error has occurred during payment.  Please contact us to ensure your order has submitted.');
  }

  $valid = uc_payonline_response_security_key($_REQUEST);
  
  if (drupal_strtolower($_REQUEST['SecurityKey']) != drupal_strtolower($valid)) {
    uc_order_comment_save($order->order_id, 0, t('Attempted unverified payonline completion for this order.'), 'admin');
    return MENU_ACCESS_DENIED;
  }


  if (!empty($_REQUEST['Address'])) {
    $order->billing_street1 = $_REQUEST['Address'];
  }
  //$order->billing_street2 = $_POST['street_address2'];
  $order->city = $_REQUEST['City'];
  //$order->billing_postal_code = $_POST['zip'];
  //$order->billing_phone = $_POST['phone'];

  /*$zone_id = db_query("SELECT zone_id FROM {uc_zones} WHERE zone_code LIKE :code", array(':code' => $_POST['state']))->fetchField();
  if (!empty($zone_id)) {
    $order->billing_zone = $zone_id;
  }*/

  $country_id = db_query("SELECT country_id FROM {uc_countries} WHERE country_name LIKE :name", array(':name' => $_REQUEST['Country']))->fetchField();
  if (!empty($country_id)) {
    $order->billing_country = $country_id;
  }

  // Save changes to order without it's completion.
  uc_order_save($order);

  /*if (drupal_strtolower($_POST['email']) !== drupal_strtolower($order->primary_email)) {
    uc_order_comment_save($order->order_id, 0, t('Customer used a different e-mail address during payment: !email', array('!email' => check_plain($_POST['email']))), 'admin');
  }*/

  if (is_numeric($_REQUEST['Amount'])) {
    $comment = t('Paid by credit card, payonline.com order #!order.', array('!order' => check_plain($_REQUEST['OrderId'])));
    uc_payment_enter($order->order_id, 'payonline', $_REQUEST['Amount'], 0, NULL, $comment);
  }
  /*else {
    drupal_set_message(t('Your order will be processed as soon as your payment clears at payonline.com.'));
    uc_order_comment_save($order->order_id, 0, t('!type payment is pending approval at payonline.com.', array('!type' => $_POST['pay_method'] == 'CC' ? t('Credit card') : t('eCheck'))), 'admin');
  }*/

  // Empty that cart...
 // uc_cart_empty($cart_id);

  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');

  $build = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  

  return $build;
}
